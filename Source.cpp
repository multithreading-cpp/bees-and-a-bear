#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <string>
#include <chrono>
#include <vector>

using namespace std;

/*� ����� ���� ����� n ���� � ���� �������, ������� ���������� ���� ������ ����,
������������ N �������. ������� ������ ������. ���� ������ �� ����������, �������
����. ��� ������ ������ �����������, ������� ����������� � ������� ���� ���, �����
���� ����� ��������. ������ ����� ����������� �������� �� ������ ������ ���� � 
������ ��� � ������. �����, ������� �������� ��������� ������ ����, ����� �������.
������� ������������� ����������, ������������ ��������� ���� � �������.*/

mutex pot_mutex;

condition_variable pot_is_full;

unsigned int pot = 0;
const unsigned int pot_capacity = 10;



void bee()
{
	while (true)
	{
		unique_lock<mutex> pot_lock(pot_mutex);
		pot_is_full.wait(pot_lock, [&]() { return pot < pot_capacity; });
		pot++;
		cout << "Bee with ID " << this_thread::get_id() << " added some honey into the pot" << endl;
		pot_lock.unlock();
		if (pot == pot_capacity)
		{
			pot_is_full.notify_one();
		}
		this_thread::sleep_for(chrono::milliseconds(100));
	}
}

void bear()
{
	while (true) 
	{
		unique_lock<mutex> pot_lock(pot_mutex);
		pot_is_full.wait(pot_lock, [&]() { return pot == pot_capacity; });
		pot = 0;
		cout << "Bear ate all the honey" << endl;
		this_thread::sleep_for(chrono::milliseconds(1000));
		pot_lock.unlock();	
		pot_is_full.notify_all();
		
	}
	
}

void main()
{
	size_t N = 5;

	vector<thread> bees;

	for (size_t i = 0; i < N; i++)
	{
		bees.push_back(thread(bee));
	}

	thread bear(bear);

	for (auto& item : bees)
	{
		item.join();
	}
	bear.join();

}